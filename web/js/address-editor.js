class AddressEditor extends React.Component
{
    constructor(props)
    {
        super(props);

        this.state = {
            isEditing: false,
            clients: [],
            client: 0,
            addresses: [],
            address: 0
        };
    }

    componentDidMount()
    {
        this.loadClients();
    }

    loadClients()
    {
        const _this = this;

        axios.get('/clients')
            .then(response => {
                const clients = response.data.map(item => {
                        return {id: item.id, name: item.firstName + ' ' + item.lastName};
                    });

                _this.setState({clients: clients});
            });
    }

    loadAddresses(id)
    {
        const _this = this;
        axios.get('/client/' + id + '/addresses')
            .then(response => {
                _this.setState({addresses: response.data});
            });
    }

    handleClientChange(event)
    {
        const clientId = event.target.value;
        this.setState({ client: clientId });
        this.loadAddresses(clientId);
    }

    customerSelect()
    {
        return (
            <label className="mr-sm-2">Customer&nbsp;
                <select className="custom-select" onChange={this.handleClientChange.bind(this)} value={this.state.client}>
                    <option key="0" value="0">--</option>
                    {this.state.clients.map(client => {
                        return <option key={client.id} value={client.id}>{client.name}</option>
                    })}
                </select>
            </label>
        );
    }


    clickAddress(addressId)
    {
        this.setState({address: addressId});
    }


    addressList()
    {
        return (
            <div className="list-group">
                {this.state.addresses.map(address => this.addressItem(address))}
                { this.state.client > 0 &&
                    <div className="list-group-item list-group-item-action flex-column align-items-start" key="0">
                        <a href="#" onClick={this.addAddress.bind(this)}><small>Add address</small></a>
                    </div>
                }
            </div>
        );
    }


    addAddress()
    {
        const addresses = this.state.addresses;
        addresses.push({
            id: 0,
            street: '',
            city: '',
            zipCode: '',
            country: '',
            isDefault: false
        });
        this.setState({
            addresses: addresses,
            address: 0
        });
    }

    cancelEdit(event)
    {
        event.preventDefault();

        if (this.state.address === 0) {
            const idx = this.addressIdx();
            const addresses = this.state.addresses;
            addresses.splice(idx, 1);
            this.setState({addresses: addresses});
        }

        this.setState({address: undefined});
    }

    setAsDefault(event)
    {
        event.preventDefault();

        for (let i = 0; i < this.state.addresses.length; i++) {
            if (this.state.addresses[i].isDefault === true) {
                this.state.addresses[i].isDefault = false;
            }
            if (this.state.addresses[i].id === this.state.address) {
                this.state.addresses[i].isDefault = true;
                this.updateAddress(this.state.addresses[i]);
            }
        }
        this.setState({address: 0});
    }

    saveAddress(event)
    {
        event.preventDefault();

        const addressIdx = this.addressIdx();
        this.updateAddress(this.state.addresses[addressIdx]);
        this.setState({address: 0});
    }

    updateAddress(address)
    {
        const _this = this;

        if (address.id === 0) {
            axios.post('/client/' + this.state.client + '/address', address)
                .then(response => {
                    const addressIdx = _this.addressIdx();
                    const addresses = _this.state.addresses;
                    addresses[addressIdx]['id'] = response.data.id;
                    _this.setState({addresses: addresses});
                });
            return;
        }
        axios.put('/address', address);
    }

    updateField(name, event)
    {
        event.preventDefault();

        const addressIdx = this.addressIdx();
        this.state.addresses[addressIdx][name] = event.target.value;
    }


    addressIdx()
    {
        for (let i = 0; i < this.state.addresses.length; i++) {
            if (this.state.addresses[i].id === this.state.address) {
                return i;
            }
        }
    }


    deleteAddress(event)
    {
        event.preventDefault();

        const _this = this;

        axios.delete('/address/' + this.state.address)
            .catch(result => {
                const idx = _this.addressIdx();
                const addresses = _this.state.addresses;
                addresses.splice(idx, 1);
                _this.setState({addresses: addresses});
            });
    }


    addressItem(address)
    {
        const addressId = this.state.address;

        if (addressId == address.id) {
            return (
                <div className="list-group-item list-group-item-action flex-column align-items-start" key={address.id}>
                    <form>
                        <div className="form-group">
                            <label>Street
                                    <input type="text" className="form-control" onChange={this.updateField.bind(this, 'street')} defaultValue={address.street} />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>City
                                <input type="text" className="form-control" onChange={this.updateField.bind(this, 'city')} defaultValue={address.city} />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>Zip code
                                <input type="text" className="form-control" onChange={this.updateField.bind(this, 'zipCode')} defaultValue={address.zipCode} />
                            </label>
                        </div>
                        <div className="form-group">
                            <label>Country
                                <input type="text" className="form-control" onChange={this.updateField.bind(this, 'country')} defaultValue={address.country} />
                            </label>
                        </div>
                        <div>
                            <a href="#" onClick={this.cancelEdit.bind(this)} className="btn btn-outline-secondary btn-sm">Cancel</a>
                            { !address.isDefault && address.id > 0 &&
                                <span>
                                    <a href="#" onClick={this.setAsDefault.bind(this)} className="btn btn-outline-secondary btn-sm">Set as default</a>
                                    <a href="#" onClick={this.deleteAddress.bind(this)} className="btn btn-outline-danger btn-sm">Delete</a>
                                </span>
                            }
                            <a href="#" onClick={this.saveAddress.bind(this)} className="btn btn-outline-primary btn-sm">Ok</a>
                        </div>
                    </form>
                </div>
                );
        }

        return (
            <div className="list-group-item list-group-item-action flex-column align-items-start" key={address.id}>
                <div className="d-flex w-100 justify-content-between">
                    <h5 className="mb-1">{address.street}</h5>
                    { address.isDefault &&
                        <h5><span className="badge badge-secondary">default</span></h5>
                    }
                </div>
                <p className="mb-1">{address.city}, {address.zipCode}, {address.country}</p>
                <a href="#" onClick={this.clickAddress.bind(this, address.id)}><small>Edit</small></a>

            </div>
            );
    }


    render()
    {
        return (
            <div>
                <form className="form-inline">
                    <div className="form-group">{this.customerSelect()}</div>
                </form>
                <p>&nbsp;</p>
                <h4 className="card-title">Adresses</h4>
                    {this.addressList()}
                </div>
            );
    }
}