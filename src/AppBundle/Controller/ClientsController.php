<?php
/**
 * Created by PhpStorm.
 * User: uj
 * Date: 17.2.9
 * Time: 14:16
 */

namespace AppBundle\Controller;



use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ClientsController extends Controller
{
    /**
     * @Route("/clients")
     * @Method("GET")
     */
    public function listAction()
    {
        $clientsList = $this->getDoctrine()
            ->getRepository('AppBundle:Client')
            ->getList();

        return new JsonResponse($clientsList);
    }
}