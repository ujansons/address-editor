<?php
/**
 * Created by PhpStorm.
 * User: uj
 * Date: 17.30.8
 * Time: 22:14
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Address;
use AppBundle\Entity\Client;
use AppBundle\Form\AddressType;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AddressController extends Controller
{
    /**
     * @Route("/client/{id}/addresses")
     * @Method("GET")
     *
     * @param Client $client
     *
     * @return JsonResponse
     */
    public function listAction(Client $client)
    {
        $em = $this->getDoctrine()->getManager();
        $addresses = $em
            ->getRepository('AppBundle:Address')
            ->findByClient($client);

        return new JsonResponse($addresses);
    }


    /**
     * @Route("/address/{id}")
     * @Method("GET")
     *
     * @param Address $address
     *
     * @return JsonResponse
     */
    public function viewAction(Address $address)
    {
        $serializer = SerializerBuilder::create()->build();

        return new JsonResponse($serializer->toArray($address));
    }

    /**
     * @Route("/client/{id}/address")
     * @Method("POST")
     *
     * @param Request $request
     * @param Client $client
     *
     * @return JsonResponse|Response
     */
    public function addAction(Request $request, Client $client)
    {
        $address = new Address();
        $data = json_decode($request->getContent(), true);
        unset($data['id']);

        $form = $this->createForm(AddressType::class, $address);
        $form->submit($data);
        if ($form->isValid()) {
            $address->setClient($client);

            $em = $this->getDoctrine()->getManager();
            $em->getRepository('AppBundle:Address')->fixDefault($address);

            $em->persist($address);
            $em->flush();

            $serializer = SerializerBuilder::create()->build();

            return new JsonResponse($serializer->toArray($address), Response::HTTP_CREATED);
        }

        $this->get('logger')->warning('Cannot add address. Invalid form.', compact('data'));

        return new Response(null, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/address")
     * @Method("PUT")
     *
     * @param Request $request
     *
     * @return JsonResponse|Response
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($request->getContent(), true);
        $id = $data['id'];
        unset($data['id']);

        $address = $em
            ->getRepository('AppBundle:Address')
            ->find($id);
        if ( ! $address) {
            $this->get('logger')->warning('Cannot update address. Invalid id.', compact('data'));

            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        $form = $this->createForm(AddressType::class, $address);
        $form->submit($data);
        if ($form->isValid()) {

            $em->getRepository('AppBundle:Address')->fixDefault($address);
            $em->flush();

            $serializer = SerializerBuilder::create()->build();

            return new JsonResponse($serializer->toArray($address), Response::HTTP_OK);
        }

        $this->get('logger')->warning('Cannot update address. Invalid form.', compact('data'));

        return new Response(null, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/address/{id}")
     * @Method("DELETE")
     */
    public function deleteAction(Address $address)
    {
        $serializer = SerializerBuilder::create()->build();
        $serializedAddress = $serializer->toArray($address);

        if ($address->getIsDefault()) {
            $this->get('logger')->warning('Cannot delete default address.', [
                'address' => $serializedAddress,
            ]);

            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($address);
        $em->flush();

        $this->get('logger')->info('Address has been removed.', ['address' => $serializedAddress]);

        return new Response(null, Response::HTTP_NOT_FOUND);
    }
}